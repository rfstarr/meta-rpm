SUMMARY = "RPM-based desktop image"

LICENSE = "MIT"

inherit rpm-image

# Default qemu to 4GB ram
QB_MEM = "-m 4096"

# Boot to multiuser gui mode
QB_KERNEL_CMDLINE_APPEND = "5"

IMAGE_INSTALL = "abattis-cantarell-fonts liberation-fonts systemd-udev glibc-langpack-en glibc-langpack-sv hostname findutils gdm procps-ng rpm gnome-terminal"

IMAGE_FEATURES += " package-management"

boot_image_post_do_rootfs () {
  # Set root password to "password"
  sed -i "s/^root:\*:/root:JU1wTQ9mH04ns:/" ${IMAGE_ROOTFS}/etc/shadow

  # Set a default UTF-8 based locale
  echo 'LANG="en_US.UTF-8"' > ${IMAGE_ROOTFS}/etc/locale.conf
}

do_rootfs[postfuncs] += "boot_image_post_do_rootfs"
