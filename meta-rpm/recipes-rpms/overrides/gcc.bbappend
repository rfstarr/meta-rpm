# This is used for gcc-native, the cross linker is in gcc-cross-host.bb

require gcc-common.inc

GCCDIR = "${nonarch_libdir}/gcc/${TARGET_ARCH}-redhat-${TARGET_OS}/${GCCBINV}"

rpmbased_post_rpm_install_prepend() {
  # We only want the binaries, otherwise it conflicts with -cross
  rm -rf ${D}${libdir} \
         ${D}${datadir}

  gcc_fixup_libgcc_so
}
