# This Makefile is not typically used as part of a normal yocto build.
# It has two kinde of targets:
#  * recipes: Used to generate the yocto recipes based on the dnf metadata.
#             Must be run before starting a yocto build.
#  * build-*: Various simple ways to trigger a build of a image, generally
#             used by developers and CI to test meta-rpm changes.

.NOTPARALLEL: # The sample images don't build in parallel

TOPDIR=.
BINDIR=$(TOPDIR)/bin
IMAGES_DIR := meta-rpm/recipes-core/images

DEFAULT_IMAGES = minimal-image test-image
ALL_IMAGES = $(DEFAULT_IMAGES) desktop-image

.PHONY: all recipes build-sample-images build-all-sample-images crossbuild-sample-images crossbuild-all-sample-images

all: recipes

recipes meta-rpm/recipes-rpms/generated:
	rm -rf $(TOPDIR)/meta-rpm/recipes-rpms/generated
	mkdir $(TOPDIR)/meta-rpm/recipes-rpms/generated
	ln -s ../overrides/files $(TOPDIR)/meta-rpm/recipes-rpms/generated/files
	$(BINDIR)/generate-recipes.py \
	   --mapping $(TOPDIR)/meta-rpm/recipes-rpms/overrides/package-mapping.yaml \
	   --dnfdir $(TOPDIR)/meta-rpm/recipes-rpms/overrides/dnf \
	   --destdir $(TOPDIR)/meta-rpm/recipes-rpms/generated/ \
	   --licenses $(TOPDIR)/meta-rpm/recipes-rpms/overrides/license_dict.json

sample-image-downloads:
	mkdir -p sample-image-downloads

sample-image-builddir: sample-image-downloads
	bash -c ". oe-init-build-env sample-image-builddir" # Create initial config
	echo 'DL_DIR = "$(shell pwd)/sample-image-downloads"' >> sample-image-builddir/conf/local.conf # Share dl dirs
	if [ `uname -m` == "x86_64" ]; then \
	   echo 'MACHINE = "qemux86-64"' >> sample-image-builddir/conf/local.conf; \
	fi
	if [ `uname -m` == "aarch64" ]; then \
	   echo 'MACHINE = "qemuarm64"' >> sample-image-builddir/conf/local.conf; \
	fi

build-%: $(IMAGES_DIR)/rpmbased-%.bb meta-rpm/recipes-rpms/generated sample-image-builddir
	@echo "Building $@"
	bash -c ". oe-init-build-env sample-image-builddir; bitbake $(notdir $(basename $<))"

build-sample-images: $(addprefix build-,$(DEFAULT_IMAGES))

build-all-sample-images: $(addprefix build-,$(ALL_IMAGES))

sample-image-crossdir: sample-image-downloads
	bash -c ". oe-init-build-env sample-image-crossdir" # Create initial config
	echo 'DL_DIR = "$(shell pwd)/sample-image-downloads"' >> sample-image-crossdir/conf/local.conf # Share dl dirs
	if [ `uname -m` == "aarch64" ]; then \
	   echo 'MACHINE = "qemux86-64"' >> sample-image-crossdir/conf/local.conf; \
	else \
	   echo 'MACHINE = "qemuarm64"' >> sample-image-crossdir/conf/local.conf; \
	fi

crossbuild-%: $(IMAGES_DIR)/rpmbased-%.bb meta-rpm/recipes-rpms/generated sample-image-crossdir
	@echo "Building $@"
	bash -c ". oe-init-build-env sample-image-crossdir; bitbake $(notdir $(basename $<))"

crossbuild-sample-images: $(addprefix crossbuild-,$(DEFAULT_IMAGES))

crossbuild-all-sample-images: $(addprefix crossbuild-,$(IMAGE_IMAGES))
